#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/gpio/consumer.h>
#include <linux/interrupt.h>
#include <linux/of.h>

static struct gpio_desc *btn;
static unsigned int irq;

static const struct of_device_id id_table[] = {
	{.compatible = "gpio-irq-module",},
	{},
};

irqreturn_t btn_irq_handler(int irq, void* dev) {
	printk(KERN_INFO "button pressed!\n");
	return IRQ_HANDLED;
}

static int device_probe(struct platform_device *pdev) {
	int ret;
	struct device *dev;
	dev = &pdev->dev;
	btn = gpiod_get(dev, "btn", GPIOD_IN); /* получаем дескриптор пина с device tree */
	irq = gpiod_to_irq(btn); /* получаем линию прерывания для пина */
	ret = request_irq(irq, btn_irq_handler, IRQF_TRIGGER_RISING, "gpio-irq-module", NULL);
	printk(KERN_INFO "device probed!\n");
	return ret;
}

static int device_remove(struct platform_device *pdev) {
	free_irq(irq, NULL);
	gpiod_put(btn);
	printk(KERN_INFO "device removed!\n");
	return 0;
}

static struct platform_driver mydrv = {
	.probe = device_probe,
	.remove = device_remove,
	.driver = {
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(id_table),
		.name = "gpio-irq",
	},
};

module_platform_driver(mydrv);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("4umber");