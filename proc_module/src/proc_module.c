#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>

#define PROC_DIRECTORY  "proc_module"
#define PROC_FILENAME   "proc_file"
#define BUFFER_SIZE     256

static char proc_buffer[BUFFER_SIZE+1];
static size_t proc_msg_length;
static size_t proc_msg_read_pos;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file_str;

static int proc_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static int proc_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);

static struct file_operations proc_fops_str = {
    .read  = proc_read,
    .write = proc_write,
};

static int proc_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset) {
	size_t left;
	
	if (proc_msg_read_pos >= proc_msg_length) {
		proc_msg_read_pos = 0;
		return 0;
	}

    if (length > (proc_msg_length - proc_msg_read_pos))
        length = (proc_msg_length - proc_msg_read_pos);

    left = raw_copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);
    proc_msg_read_pos += length - left;
	return length - left;
}

static int proc_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset) {
	size_t msg_length;
    int left;
	msg_length = (length < BUFFER_SIZE ? length : BUFFER_SIZE);
    left = strncpy_from_user(proc_buffer, buffer, msg_length);

    if (left != -EFAULT) {
		proc_msg_length = left;
    	proc_msg_read_pos = 0;
	}
    return left;
}

static int create_procdir(void) {
	proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;
    proc_file_str = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops_str);
    if (NULL == proc_file_str)
        return -EFAULT;
    return 0;
}

void cleanup_procdir(void) {
	if (proc_file_str)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file_str = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}

int __init proc_init(void) {
	if (create_procdir()) {
		cleanup_procdir();
		return -1;
	}
	proc_msg_length = 0;
	proc_msg_read_pos = 0;
	printk(KERN_INFO "=== module installed ===\n");
	return 0;
}

void __exit proc_exit(void) {
	cleanup_procdir();
	printk(KERN_INFO "=== module removed === \n");
}

module_init(proc_init);
module_exit(proc_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("4umber");
