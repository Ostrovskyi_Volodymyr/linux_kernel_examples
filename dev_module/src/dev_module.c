#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

static ssize_t dev_read(struct file *file, char __user *buf, size_t count, loff_t *ppos) {
	int len;
	len = strlen(file->private_data);
	if (count < len) {
		return -EINVAL;
	}

	if (*ppos != 0) {
		printk(KERN_INFO "=== read return: 0\n");
		return 0;
	}
	if (raw_copy_to_user(buf, file->private_data, len)) {
		return -EINVAL;
	}
	*ppos = len;
	printk("=== read return: %d\n", len);
	return len;
}

static ssize_t dev_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos) {
	raw_copy_from_user(file->private_data, buf, count);
	((char*)file->private_data)[count] = '\0';
	return count;
}

static int dev_open(struct inode *n, struct file *f) {
	f->private_data = kmalloc(200 * sizeof(char), GFP_KERNEL);
	if (f->private_data == NULL) {
		return -ENOMEM;
	}
	strcpy(f->private_data, "not initialized\n");
	return 0;
}

static int dev_release(struct inode *n, struct file *f) {
	kfree(f->private_data);
	return 0;
}

static const struct file_operations fops = {
	.owner = THIS_MODULE,
	.read = dev_read,
	.write = dev_write,
	.open = dev_open,
	.release = dev_release,
};

#define DEVICE_FIRST 0 // первый minor
#define DEVICE_COUNT 3 // всего миноров (имен)
#define MODNAME "dev_module"
#define DEVNAME "devm"

static struct cdev hcdev;
static struct class *dev_class;
static uint8_t major;

int __init dev_init(void) {
	dev_t dev, i;
	int ret;
	// Получаем major номер динамически, попутно выделив несколько миноров
	ret = alloc_chrdev_region(&dev, DEVICE_FIRST, DEVICE_COUNT, MODNAME);
	major = MAJOR(dev); // Сохраняем major для корректной очистки
	if (ret < 0) {
		printk(KERN_ERR "=== Can not register char device region!\n");
		goto err;
	}
	// Заполняем cdev
	cdev_init(&hcdev, &fops);
	hcdev.owner = THIS_MODULE;
	ret = cdev_add(&hcdev, dev, DEVICE_COUNT);
	if (ret < 0) {
		unregister_chrdev_region(MKDEV(major, DEVICE_FIRST), DEVICE_COUNT);
		printk(KERN_ERR "=== Can not add char device!\n");
		goto err;
	}
	dev_class = class_create(THIS_MODULE, "devmod_class");
	for (i = 0; i < DEVICE_COUNT; ++i) {
		dev = MKDEV(major, DEVICE_FIRST + i);
		device_create(dev_class, NULL, dev, NULL, "mydevice!%s_%d", DEVNAME, i);
	}
	printk(KERN_INFO "=== module installed %d:%d ===\n", MAJOR(dev), MINOR(dev));
err:
	return ret;
}

void __exit dev_exit(void) {
	dev_t dev;
	int i;
	for (i = 0; i < DEVICE_COUNT; ++i) {
		dev = MKDEV(major, DEVICE_FIRST + i);
		device_destroy(dev_class, dev);
	}
	class_destroy(dev_class);
	cdev_del(&hcdev);
	unregister_chrdev_region(MKDEV(major, DEVICE_FIRST), DEVICE_COUNT);
	printk(KERN_INFO "=== module removed ===\n");
}

module_init(dev_init);
module_exit(dev_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("4umber");