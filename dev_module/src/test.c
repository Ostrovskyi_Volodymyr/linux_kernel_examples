#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static const char *str1 = "222222";
static const char *str2 = "777";

int main(void) {
	int fd1, fd2, len;
	char buf[20];
	fd1 = open("/dev/mydevice/devm_0", O_RDWR);
	fd2 = open("/dev/mydevice/devm_0", O_RDWR);
	len = strlen(str1);
	write(fd1, str1, len + 1);
	len = strlen(str2);
	write(fd2, str2, len + 1);
	len = read(fd2, buf, 20);
	buf[len] = '\0';
	printf("Second descriptor: %s\n", buf);
	len = read(fd1, buf, 20);
	buf[len] = '\0';
	printf("First descriptor: %s\n", buf);
	close(fd1);
	close(fd2);
	return 0;
}