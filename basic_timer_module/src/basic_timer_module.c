#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/jiffies.h>
#include <linux/timer.h>

static struct timer_list btimer;
static uint32_t secs;

void basic_timer_handler(struct timer_list *t) {
	secs++;
	printk(KERN_INFO "%d seconds\n", secs);
	mod_timer(&btimer, jiffies + HZ);
}

int __init timer_mod_init(void) {
	timer_setup(&btimer, basic_timer_handler, 0);
	mod_timer(&btimer, jiffies + HZ);
	return 0;
}

void __exit timer_mod_exit(void) {
	del_timer(&btimer);
}

module_init(timer_mod_init);
module_exit(timer_mod_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("4umber");